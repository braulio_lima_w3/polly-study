# Simple project to test **Polly**

Here are a few things to note:

## Timeout answers the question:

> If it's taking too long... 😰

Code Example:

```cs
Policy.Timeout(10, TimeoutStrategy.Pessimistic).Execute(()=>{
    // long lasting action
});
```

## Circuit Breaker:

> Too many errors happened! 😱 Abort! Stop retrying!

Code Example:

```cs
var cbPolicy = Policy.Handle<Exception>()
    .CircuitBreaker(exceptionsAllowedBeforeBreaking:5, TimeSpan.FromSeconds(30));

for(var i = 0; i < 20; i++)
{
    cbPolicy.Execute(()=>MethodThatWillFail());
}
```

## Retry:

> Keep doing it until it works 😠

Code Example:

```cs
Policy.Handle<Exception>()
    .Retry(10)
    .Execute(()=>{
        // method that will fail
    })
```

## Wait And Retry:

> Keep doing it until it works 😠. But only every now and then. It might magically start working by then. 🤔

Code Example:

```cs
Policy.Handle<Exception>()
    .WaitAndRetry(5, (time)=>TimeSpan.FromSeconds(Math.Pow(2, time)))
    .Execute(()=>{
        // method that will fail
    })
```

