
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace PollyConsoleApp.Interface
{
    public static class DictionaryExt
    {
        public static TResult GetValueAsOrDefault<Tkey, TValue, TResult>(this IDictionary<Tkey, TValue> dic, Tkey key, Type type, TResult def)
        {
            if(def == null){
                throw new Exception("The default value cannot be null");
            }
            TResult output = def;
            if(dic.ContainsKey(key))
            {
                TValue value;
                dic.TryGetValue(key, out value);
                if(value != null && value.GetType().IsAssignableFrom(type))
                {
                    dynamic temp = value;
                    output = (TResult) temp;
                }
            }
            return output;
        }
        
        public static void AddOrReplace<TA, TB>(this IDictionary<TA, TB> dictionary, TA key, TB value){
            if(dictionary.ContainsKey(key)) dictionary.Remove(key);
            dictionary.Add(key, value);
        }
    }

    public class NiceConsoleKey
    {
        public char Key { get; private set; }
        public string Name { get; private set; }

        public NiceConsoleKey(char key)
        {
            Key = key;
        }

        public NiceConsoleKey(char key, string name)
        {
            Key = key;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if(obj == null || obj.GetType() != typeof(NiceConsoleKey)) return false;
            return ((NiceConsoleKey) obj).Key == this.Key;
        }

        public override int GetHashCode()
        {
            return this.Key.GetHashCode();
        }

        public override string ToString()
        {
            if(string.IsNullOrEmpty(Name))
            {
                return $"[{Key}]";
            }
            return $"[{Key}] - {Name}";
        }

    }

    public class NiceInterface<TOptionType>
    {
        private IDictionary<string, object> ContextualData { get; set; } = new Dictionary<string, object>();
        public delegate void ExecuteWithContext(IDictionary<string, object> ctx);
        private IDictionary<TOptionType, ExecuteWithContext> OptionsAndFunctions { get; set; } = new Dictionary<TOptionType, ExecuteWithContext>();
        private Func<IDictionary<string, object>, TOptionType> ObtainKeyFunction { get; set; } = null;
        private bool running = true;
        private readonly TOptionType exitKey;

        public NiceInterface(TOptionType exitKey)
        {
            this.exitKey = exitKey;
            AddFunctionality(exitKey, (context) => running = false);
        }

        public NiceInterface<TOptionType> AddFunctionality(TOptionType key, ExecuteWithContext context)
        {
            if(OptionsAndFunctions.ContainsKey(key))
            {
                throw new Exception("Already contains key");
            }
            if(context == null)
            {
                throw new Exception("Cannot be null");
            }
            OptionsAndFunctions.Add(key, context);
            return this;
        }

        public NiceInterface<TOptionType> ObainKey(Func<IDictionary<string, object>, TOptionType> function)
        {
            this.ObtainKeyFunction = function;
            return this;
        }

        public void Run()
        {
            if(ObtainKeyFunction == null) throw new Exception("User input function is null. Cannot proceed.");
            while(running)
            {
                Console.Clear();

                Console.WriteLine("Your options are:\n");
                foreach(var i in OptionsAndFunctions.Keys)
                {
                    Console.WriteLine(i.ToString());
                }

                Console.WriteLine("\n");
                Console.Write("Choose an option: ");

                TOptionType option = ObtainKeyFunction.Invoke(ContextualData);

                Console.WriteLine("");

                try{
                    OptionsAndFunctions.GetValueAsOrDefault(
                            option,
                            typeof(ExecuteWithContext),
                            (ExecuteWithContext) ((data)=>{Console.WriteLine("What you chose is not a valid option");})
                        ).Invoke(ContextualData);
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Error occured while executing action {option.ToString()}");
                    Console.WriteLine($"Message: {ex.Message}");
                }

                if(!option.Equals(exitKey))
                {
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
            }
        }
    }
}