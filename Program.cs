﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Polly;
using Polly.Timeout;
using PollyConsoleApp.Interface;

namespace PollyConsoleApp
{
    class Program
    {

        private static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            const string LinkField = "link_value";
            const string LastResultField = "last_result";
            new NiceInterface<NiceConsoleKey>(new NiceConsoleKey('q', "Exit the application"))
                .AddFunctionality(
                    new NiceConsoleKey('t', "Polly Timeout"),
                    ctx => {
                        if(!ctx.ContainsKey(LinkField)){
                            System.Console.WriteLine("Link field missing from context.");
                            return;
                        }
                        Policy.Timeout(TimeSpan.FromSeconds(10), TimeoutStrategy.Pessimistic)
                            .Execute(()=>{
                                var gitGud = GetShortenedLink(ctx.GetValueAsOrDefault(LinkField, typeof(string), ""));
                                gitGud.Wait();
                                var result = gitGud.GetAwaiter().GetResult();
                                ctx.AddOrReplace(LastResultField, result);
                            });
                    })
                .AddFunctionality(
                    new NiceConsoleKey('b', "Polly Circuit Breaker"),
                    ctx => {
                        var cbPolicy = Policy
                            .Handle<Exception>()
                            .CircuitBreakerAsync(
                                5,
                                TimeSpan.FromSeconds(30),
                                (ex, timer)=>{
                                    Console.WriteLine("An error happened...");
                                },
                                ()=>{
                                    Console.WriteLine("Resetting...");
                                },
                                ()=>{ Console.WriteLine("It's half open. The next failure will close the circuit."); });
                            for(var i = 0; i < 20; i ++)
                            {
                                Console.Write($"Trial {i+1}/20: ");
                                try{
                                    cbPolicy.ExecuteAsync(async ()=>{ await ExceptionThrower(500); }).Wait();
                                }
                                catch(Exception e)
                                { 
                                    Console.WriteLine(e.Message);
                                }
                                Thread.Sleep(5000);
                            }
                    })
                .AddFunctionality(
                    new NiceConsoleKey('w', "Wait and retry"),
                    ctx => {
                        if(!ctx.ContainsKey(LinkField)){
                            System.Console.WriteLine("Link field missing from context.");
                            return;
                        }
                        Policy.Handle<Exception>()
                            .WaitAndRetryAsync(5, time => TimeSpan.FromSeconds(5), (ex, time)=>Console.WriteLine("Error trown. Trying again..."))
                            .ExecuteAsync(async ()=>{
                                ThrowRandom();
                                ctx.AddOrReplace(LastResultField, await GetShortenedLink(ctx.GetValueAsOrDefault(LinkField, typeof(string), "")));
                            })
                            .Wait();
                    })
                .AddFunctionality(
                    new NiceConsoleKey('c', "Clear context"),
                    ctx => {
                        ctx.Clear();
                    })
                .AddFunctionality(
                    new NiceConsoleKey('i', "Info/Read Context info"),
                    ctx => {
                        foreach(var key in ctx.Keys){
                            object val = null;
                            ctx.TryGetValue(key, out val);
                            Console.WriteLine($"{key} => {val.ToString()}");
                        }
                    }
                )
                .AddFunctionality(
                    new NiceConsoleKey('a', "Add Link To Shorten"),
                    ctx => {
                        Console.Write("Give me a link to shorten, please: ");
                        if(ctx.ContainsKey(LinkField)) ctx.Remove(LinkField);
                        ctx.Add(LinkField, Console.ReadLine());
                    })
                .ObainKey((ctx)=>{
                    return new NiceConsoleKey(Console.ReadKey().KeyChar);
                })
                .Run();
        }

        private static Random random = new Random(DateTime.UtcNow.Subtract(new DateTime(1970,1,1,0,0,0,DateTimeKind.Utc)).Milliseconds);

        public static void ThrowRandom()
        {
            if(random.Next() % 2 == 1) ThrowError();
        }

        public static void ThrowError()
        {
            throw new Exception("AN ERROR HAPPENED! Oh no! 😱");
        }

        public static async Task ExceptionThrower(int milliseconds)
        {   
            await Task.Run(()=>{
                Console.WriteLine("Waiting...");
                Thread.Sleep(milliseconds);
                ThrowError();
            });
        }

        public static async Task<string> GetShortenedLink(string toSend)
        {
            var keypair = new List<KeyValuePair<string, string>>();
            keypair.Add(new KeyValuePair<string, string>("location", toSend));
            var send = new FormUrlEncodedContent(keypair);

            HttpResponseMessage result = await client.PostAsync(
                "http://localhost:3000/shorten",
                send);

            if(!result.IsSuccessStatusCode){
                throw new Exception("Shortening not possible.");
            }

            var dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(await result.Content.ReadAsStringAsync());
            Console.WriteLine(dic.GetValueOrDefault("path", "Nope. Sorry."));
            return dic.GetValueOrDefault("path", "Nope. Sorry.");
        }
    }
}
