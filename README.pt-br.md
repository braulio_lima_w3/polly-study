# Um projeto simples para testar **Polly**

Alguns pontos para notar:

## *Timeout* responde a esse problema:

> Está demorando de mais... 😰

Exemplo:

```cs
Policy.Timeout(10, TimeoutStrategy.Pessimistic).Execute(()=>{
    // Alguma acao que demora mais que 10s
});
```

## *Circuit breaker*:

> Muitos errors aconteceram! 😱! Pare! Pare de tentar!

Exemplo:

```cs
var cbPolicy = Policy.Handle<Exception>()
    .CircuitBreaker(exceptionsAllowedBeforeBreaking:5, TimeSpan.FromSeconds(30));

for(var i = 0; i < 20; i++)
{
    cbPolicy.Execute(()=>MetodoQueVaiFalhar());
}
```

## *Retry*/Tentar novamente:

> Vou tentar até funcionar! 😠

Exemplo:

```cs
Policy.Handle<Exception>()
    .Retry(10)
    .Execute(()=>{
        // metodo que vai falahar
    })
```

## Esprar e tentar novamente:

> Vou tentar até funcionar 😠. Mas vou esperar um pouco entre cada tentativa. Talvez o negócio volte a funcionar até la. 🤔

Exemplo:

```cs
Policy.Handle<Exception>()
    .WaitAndRetry(5, (time)=>TimeSpan.FromSeconds(Math.Pow(2, time)))
    .Execute(()=>{
        // metodo que vai falahar
    })
```